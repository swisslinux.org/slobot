image_version = "latest"
image_name = "slobot"
.PHONY: build-image save-image clean

all: build-image

build-image:
	@echo "Build the container image"
	podman build . -t $(image_name):$(image_version)

save-image:
	@echo "Save image as an archive"
	mkdir -p images/
	podman save -o images/$(image_name)_$(image_version).tar $(image_name):$(image_version)
	@echo "Compress the image"
	gzip images/$(image_name)_$(image_version).tar

clean:
	@echo "Delete container image"
	podman rmi $(image_name):$(image_version)
