from setuptools import setup, find_packages
import subprocess

#Get version
version = subprocess.getoutput('git describe --tags').split('-')[0]


setup(
        name='slobot',
        version=version,
        description="SwissLinux.Org's IRC Bot",
        author=[
            'Maxime Augier',
            'Sébastien Gendre',
        ],
        author_email=[
            'max@xolus.net',
            'seb@k-7.ch',
        ],
        packages=['slobot'],
        license='GPL2',
        install_requires=[
            'irc',
            'pyyaml',
        ],
        entry_points={
                'console_scripts': ['slobot = slobot:main'],
        }
)
